EasyPR-Java(部分摘抄于coca_le_cola的EasyPR-Java项目，重点整理“下载安装和eclipse配置”部分)
======

Introduction 简介
------------
EasyPR-Java是[liuruoze/EasyPR](https://github.com/liuruoze/EasyPR) 的Java版本。
EasyPR是一个中文的开源车牌识别系统，其目标是成为一个简单、高效、准确的车牌识别引擎。

假设我们有如下的原始图片，需要识别出中间的车牌字符与颜色：

![EasyPR 原始图片](res/image/test_image/plate_recognize.jpg)

经过EasyPR的第一步处理车牌检测（PlateDetect）以后，我们获得了原始图片中仅包含车牌的图块：

![EasyPR 车牌](res/image/test_image/chars_segment.jpg)

接着，我们对图块进行OCR过程，在EasyPR中，叫做字符识别（CharsRecognize）。我们得到了一个包含车牌颜色与字符的字符串：

“蓝牌：苏EUK722”
 
Release Notes 更新
------------
[v0.1 first release](https://github.com/mumu10/EasyPR-Java/releases)
This is the first release can recognize plate in some simple cases.

Downloads and Installation 下载安装
------------
Git克隆一份拷贝到你本机或者直接下载zip压缩。EasyPR-Java 使用maven管理，需事先安装maven,并在eclipse配置变量M2_REPO，指向本地maven配置库。
另外本项目运行时需要安装opencv-2.4.11.exe,请从http://opencv.org/官网下载后安装。安装过程其实就只是解压。

#### Eclipse
1.在项目根目录下执行maven eclipse:eclipse 然后使用Eclipse直接导入EasyPR的目录。
2.指定src和res为源代码目录
3.在project--properies--java-build-path中，选中opencv*.jar,指定native liberary目录为$%OPENCV_HOME%\build\java\x64
4.有时报错Failed to write core dump. Minidumps are not enabled by default on client versions of Windows，尚未找到对虚拟机应参数配置，可解决。
5.GeneralTest是用于批量测试的，但openCV不支持文件名中有中文字符。文件名称中有中文时会报错：
  OpenCV Error: Assertion failed (scn == 3 || scn == 4) in cv::cvtColor, file ..\..\..\..\opencv\modules\imgproc\src\color.cpp, line 3739
  所以批量测试时，可使用general_test2目录
6.jdk必须和系统位数相同，即同时使用64或32的。

Required Software
------------
本版本在以下平台测试通过：
* windows10 64bit
* Eclipse Java EE IDE for Web Developers. Version: Mars.2 Release (4.5.2)
* jdk1.7-64时运行报错
* junit 4
