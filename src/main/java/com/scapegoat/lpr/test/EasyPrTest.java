package com.scapegoat.lpr.test;

import static org.bytedeco.javacpp.opencv_highgui.imread;

import java.io.File;
import java.util.Vector;

import org.bytedeco.javacpp.opencv_core.Mat;
import com.scapegoat.lpr.core.CharsIdentify;
import com.scapegoat.lpr.core.CharsRecognise;
import com.scapegoat.lpr.core.CoreFunc;
import com.scapegoat.lpr.core.PlateDetect;
import com.scapegoat.lpr.core.PlateLocate;
import org.junit.Test;

/**
 * @author lin.yao
 */
public class EasyPrTest {

    @Test
    public void testPlateRecognise() {

        System.out.println(System.getProperty("java.library.path"));
        org.bytedeco.javacpp.Loader t;

        //String imgPath = "res/image/test_image/Y44748.jpg";
        String imgPath = "res/image/test_image/chars_recognise_suEUK722.jpg";

        File f = new File(imgPath);
        if (f.exists() && f.isFile()) {
            System.out.println(f.length());
        } else {
            System.out.println("file doesn't exist or is not a file");
        }

        Mat src = imread(imgPath);
        PlateDetect plateDetect = new PlateDetect();
        plateDetect.setPDLifemode(true);
        Vector<Mat> matVector = new Vector<Mat>();
        int recoginzeResult = plateDetect.plateDetect(src, matVector);
        if (0 == recoginzeResult) {
            CharsRecognise cr = new CharsRecognise();

            for (int i = 0; i < matVector.size(); ++i) {
                String result = cr.charsRecognise(matVector.get(i));
                System.out.println("Chars Recognised: " + result);
            }
        } else {
            System.out.println(recoginzeResult);
        }
    }

    @Test
    public void testPlateDetect() {
        String imgPath = "res/image/test_image/Y44748.jpg";

        Mat src = imread(imgPath);
        PlateDetect plateDetect = new PlateDetect();
        plateDetect.setPDLifemode(true);
        Vector<Mat> matVector = new Vector<Mat>();
        if (0 == plateDetect.plateDetect(src, matVector)) {
            for (int i = 0; i < matVector.size(); ++i) {
                CoreFunc.showImage("Plate Detected", matVector.get(i));
            }
        }
    }

    @Test
    public void testPlateLocate() {
        String imgPath = "res/image/test_image/test.jpg";

        Mat src = imread(imgPath);

        PlateLocate plate = new PlateLocate();
        plate.setDebug(true);
        plate.setLifemode(true);

        Vector<Mat> resultVec = plate.plateLocate(src);

        int num = resultVec.size();
        for (int j = 0; j < num; j++) {
            // showImage("Plate Located " + j, resultVec.get(j));
        }

        return;
    }

    @Test
    public void testCharsRecognise() {
        String imgPath = "res/image/test_image/chars_recognise_huAGH092.jpg";
        //String imgPath = "res/image/test_image/Y44748.jpg";
        Mat src = imread(imgPath);
        CharsRecognise cr = new CharsRecognise();
        cr.setCRDebug(true);
        String result = cr.charsRecognise(src);
        System.out.println("Chars Recognised: " + result);
    }

    @Test
    public void testColorDetect() {
        String imgPath = "res/image/test_image/core_func_yellow.jpg";

        Mat src = imread(imgPath);

        CoreFunc.Color color = CoreFunc.getPlateType(src, true);
        System.out.println("Color Deteted: " + color);
    }

    @Test
    public void testProjectedHistogram() {
        String imgPath = "res/image/test_image/chars_identify_E.jpg";

        Mat src = imread(imgPath);
        CoreFunc.projectedHistogram(src, CoreFunc.Direction.HORIZONTAL);
    }

    @Test
    public void testCharsIdentify() {
        String imgPath = "res/image/test_image/chars_identify_E.jpg";

        Mat src = imread(imgPath);
        CharsIdentify charsIdentify = new CharsIdentify();
        String result = charsIdentify.charsIdentify(src, false, true);
        System.out.println(result);
    }

}
